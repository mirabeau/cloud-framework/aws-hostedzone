AWS Hosted Zone
===============
This role is part of the [Mirabeau Cloud Framework](https://gitlab.com/mirabeau/cloud-framework/)

Create Cloudformation stack for Route53 hostedzone. And optionally associate additional VPC's.

A test playbook has been supplied which creates 1 VPC and then creates an internal and an external hostedzone for it.
You can run this playbook using the following command:
```bash
ansible-playbook aws-hostedzone/tests/test.yml --inventory aws-hostedzone/tests/inventory.yml
```
This command should run outside of the role dir and requires the aws-vpc role to be in the same root dir as well.

Requirements
------------
Ansible version 2.5.4 or higher
Python 2.7.x

Required python modules:
* boto
* boto3
* awscli

Dependencies
------------
* aws-vpc or vpc outputs supplied with group_vars:
```yaml
## Static VPC Configuration
vpc_stack:
  stack_description:
    stack_name: <vpc stack name>
  stack_outputs:
    VpcId: <vpc-id>
    AclPublicID: <acl-id>
    AclPrivateID: <acl-id>
    SubnetsPublic: <subnet-id1>,<subnet-id2>,<subnet-id3>
    SubnetsPrivate: <subnet-id1>,<subnet-id2>,<subnet-id3>
    SubnetPrivateID1: <subnet-id1>
    SubnetPrivateID2: <subnet-id2>
    SubnetPrivateID3: <subnet-id3>
    SubnetPrivateCIDR1: <cidr-1> #(eg: 10.111.20.0/24
    SubnetPrivateCIDR2: <cidr-2>
    SubnetPrivateCIDR3: <cidr-3>
```
Role Variables
--------------
### _General_
The following params should be available for Ansible during the rollout of this role:
```yaml
aws_region      : <aws region, eg: eu-west-1>
owner           : <owner, eg: mirabeau>
account_name    : <aws account name>
account_abbr    : <aws account generic environment>
environment_type: <environment>
environment_abbr: <environment abbriviation>
```

Role Defaults
--------------
```yaml
cloudformation_tags  : {}
tag_prefix           : "mcf"
aws_hostedzone_params:
  debug: False
  zones: []
```

Example Playbook
----------------
Create internal and external hosted zones for test VPC.
One internal zone dta.com has associated additional vpc's along the default vpc.
So that the attached vpc's can resolve or that a recursor can resolve it.
```yaml
---
- hosts: localhost
  connection: local
  gather_facts: False
  vars:
    tag_prefix      : "mcf"
    aws_region      : "eu-west-1"
    owner           : "myself"
    account_name    : "my-dta"
    environment_type: "test"
    environment_abbr: "tst"
    internal_route53_domainname: "{{ environment_abbr }}.{{ owner }}.cloud"
    external_route53_domainname: "{{ environment_abbr }}.{{ owner }}.cloud"
    aws_vpc_params  :
      network     : "10.129.2.0/24"
      domain_names: "{{ internal_route53_domainname }}"
    aws_hostedzone_params:
      zones:
        - stack_name    : "{{ environment_abbr }}-internal-route53"
          domainname    : "{{ internal_route53_domainname }}"
          is_internal   : 1
        - stack_name    : "{{ environment_abbr }}-internal-route53"
          domainname    : "dta.com"
          assoc_add_vpcs: ["vpc-123","vpc-456","vpc-678"]
          is_internal   : 1
        - stack_name    : "{{ environment_abbr }}-external-route53"
          domainname    : "{{ external_route53_domainname }}"
          is_internal   : 0
  roles:
    - aws-vpc
    - aws-hostedzone
```
License
-------
GPLv3

Author Information
------------------
Lotte-Sara Laan <llaan@mirabeau.nl>

